// Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата". 
// Створити вкладений об'єкт - "Додаток". 
// Створити об'єкт "Додаток",  зі вкладеними об'єктами, "Заголовок, тіло, футер, дата". 
// Створити методи для заповнення та відображення документа. використовуючі оператор in


const Document = {
    header: "",
    body: "",
    footer: "",
    date: "",
    
    addendum: {
            header: {
                h: ""
            },
            body: {
                b: ""
            },
            footer: {
                f: ""
            },
            date: {
                d: ""
            }
    },
    
    fill: function() {        
        let caller = this;
        let isInnerObject = false;
        fillProperties(caller, isInnerObject, "");

        function fillProperties(o, isInnerObject, parent) {
            for (let prop in o) {
                if (typeof o[prop] === "object") {
                    isInnerObject = true;
                    fillProperties(o[prop], isInnerObject, prop);
                    isInnerObject = false;
                    continue;
                };
                if (typeof o[prop] === "function") {continue;}
                
                o[prop] = isInnerObject ? prompt(`Введите значение свойства ${prop} вложенного объекта ${parent}`) : prompt("Введите значение свойства " + prop);
            }
        }  
    },

    print: function() {
        let caller = this;
        printProperties(caller, "");

        function printProperties(o, prefix) {
            for (let prop in o) {
                if (typeof o[prop] === "object") {
                    document.write(prefix.slice(3) + prop + "{}:<br>");
                    printProperties(o[prop], "------");
                    continue;
                }
                if (typeof o[prop] === "function") {continue;}
                
                document.write(prefix + prop + ": " + o[prop] + "<br>");
            }
        }
    }
};

Document.fill();
Document.print();



